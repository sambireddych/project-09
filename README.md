##Group 09



##Our Team
##Sambi Reddy Chanimella -- (Customer,OrderLineItem - view, controller, model)
##Sandeep Devineni -- (product-view,controller,model,data)
##Roshan Thalal -- (Order-view, controller, model, data)

##Get Started
You can navigate through three pages.

1.Home page - Home page shows products.
2.Product - A page in which you find latest products.
3.Order - Create or manage Existing orders.
4.Order Line - Manage line items in your order.
5.Customers - Manage customer profile who shares the same account.
6.About us - A little info about our core team.
7.Contact us page- A page which has a little info to contact our organization for any queries and in simple electronic mail form.
##LINK to the demo
https://sambireddy-ao4.herokuapp.com/


##How to use this on your local machine
Fork repo and Clone it to your local machine.
Once this is done make sure you download the latest version of node and npm.
Navigate to the path where you have downloaded on the local machine.
open a command window here.
Install all the dependencies in that folder by doing an npm install on it. ["type : npm install"]
Once this is done type "node app.js" or "nodemon" or "npm start" to start the application.
Now the project should be started. Go to chrome and type localhost:8089/


