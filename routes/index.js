const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
    LOG.debug('Request to /')
    res.render('index.ejs', { title: 'Express App' })
})
router.get("/index", function (req, res) {
    //res.sendFile(path.join(__dirname + '/assets/index.html'))
    res.render("index.ejs")
})
router.get("/customer", function (req, res) {
    //res.sendFile(path.join(__dirname + '/assets/index.html'))
    res.render("customers/index.ejs")
})

router.get("/product", function (req, res) {
    res.render("products/index.ejs")
})

router.get("/order", function (req, res) {
    res.render("order/index.ejs")
})


router.get("/orderLine", function (req, res) {
    res.render("orderLineItems/index.ejs")
})

router.get("/about", function (req, res) {
    res.render("about.ejs")
})

router.get("/contact", function (req, res) {
    res.render("contact.ejs")
})

router.use('/customer', require('../controllers/customer.js'));
router.use('/product', require('../controllers/product.js'))
router.use('/orderLine', require('../controllers/orderLineItems.js'));
router.use('/order', require('../controllers/order.js'))
router.get(function (req, res) {
    res.render('404')
})
LOG.debug('END routing')
module.exports = router